# !/usr/bin/python
# -*- coding=utf-8 -*-
# Python 3

#    Copyright (C) 2015 by
#    Ranaivo Razakanirina <ranaivo.razakanirina@atety.com>
#    All rights reserved.
#    BSD license.

import numpy as np
import numpy.linalg as la

def array2dataplot(x, y):
    """This function transforms two arrays having the same dimension x and y
    that are necessaries to be plotted to flat arrays able to be plotted.
    Eg:
    x = [0.05, 0.02, 0.03]
    y = [[2, 3, 7, 9], [-1, 6, 3], [7, 2]]
    The returned arrays are:
    datax = [0.05, 0.05, 0.05, 0.05, 0.02, 0.02, 0.02, 0.03, 0.03]
    datay = [2, 3, 7, 9, -1, 6, 3, 7, 2]
    """
    datax = sum(list(map(lambda x, y: [x] * len(y), x, y)), [])
    datay = sum(y, [])
    return datax, datay
    

def getuniquesecondlevelkeylist(x):
    """This function returns the list of unique values of the second level key 
    of a dict.
    Let us consider the following dict.
    x = {0: {1: 50, 2: 20}, 1: {1: 300}}
    
    This function returns
    [1, 2]
    """
    y = list()
    for x_index in x:
        for y_index in x[x_index]:
            y.append(y_index)
    y = list(set(y)) 
    return y


def countuniquevalue(x, sensitivity=10000):
    """
    """
    k = list(map(lambda x: round(sensitivity*x), x))
    return len(set(k))


def rows_sum_norm(A):
    """This function returns the normalized version of row-sum
    of a square matrix.

    Parameters
    ----
    A: numpy array
      An input square matrix
    Returns
    ----
    Return diag^(-1)(A1).A
    """
    n_rows = A.shape[0]
    one = np.ones(n_rows)
    return np.dot(la.inv(np.diag(np.dot(A, one))), A)


def metafilename(initial_conditions_c=(0.8, 0.1, 0.1),
                 initial_conditions_g=(0.8, 0.1, 0.1),
                 step=0.005):
    """
    """
    complete_meta = ('N1_meta_c123_'
                          + str(round(initial_conditions_c[0], 5)) + '_' 
                          + str(round(initial_conditions_c[1], 5)) + '_'
                          + str(round(initial_conditions_c[2], 5)) + '_'
                          + 'g123_'
                          + str(round(initial_conditions_g[0], 5)) + '_' 
                          + str(round(initial_conditions_g[1], 5)) + '_'
                          + str(round(initial_conditions_g[2], 5))
                          + '_s'
                          + str(step))
    complete_meta_folder  = complete_meta + '/'
    complete_meta_filename = complete_meta + '.json'
    
    return complete_meta, complete_meta_folder, complete_meta_filename


def metafilenamecomplete(networkname='N1',
                         initial_conditions_c=(0.8, 0.1, 0.1),
                         initial_conditions_g=(0.8, 0.1, 0.1),
                         step=0.005):
    """
    """
    c_indices = 'c' + "".join(map(lambda x:str(x), list(range(0,len(initial_conditions_c),1))))
    g_indices = 'g' + "".join(map(lambda x:str(x), list(range(0,len(initial_conditions_g),1))))
    c_ini = "_".join(map(lambda x: str(round(x,5)),initial_conditions_c))
    g_ini = "_".join(map(lambda x: str(round(x,5)),initial_conditions_g))
    
    complete_meta = (networkname + '_meta_'
                    + c_indices + '_'
                    + c_ini + '_'
                    + g_indices + '_'
                    + g_ini
                    + '_s'
                    + str(step))
    complete_meta_folder  = complete_meta + '/'
    complete_meta_filename = complete_meta + '.json'
    
    return complete_meta, complete_meta_folder, complete_meta_filename
