# !/usr/bin/python
# -*- coding=utf-8 -*-
# Python 3

#    Copyright (C) 2015 by
#    Ranaivo Razakanirina <ranaivo.razakanirina@atety.com>
#    All rights reserved.
#    BSD license.

import roohypy as rp
import numpy as np
import json as json

def getbifurcationdata(parameter='alpha',
                    longtermpath='',
                    fixedparameter=200):
    """
    """
    with open(longtermpath, 'r') as longtermfile:
        data = json.load(longtermfile)
        
        # Transform the initial json with string keys to numeric keys
        data_numeric = {}
        for key1 in data:
            data_numeric[int(key1)] = {}
            for key2 in data[key1]:
                data_numeric[int(key1)][int(key2)] = data[key1][key2]

        # Get sorted list of alphas in float type
        # (normally these values are unique).
        # Values are transformed in real
        alphas = sorted(data_numeric)

        # Get sorted list of mus. These values are also unique.
        # Each alpha has the same set of mu.
        # Thus, taking only a particular alpha (e.g the first) returns
        # the corresponding mus.
        mus = data_numeric[alphas[0]]
        mus = sorted(mus)
        
        try:
            y = list()
            if parameter == 'alpha':
                x = alphas
                for a in alphas:
                    if a not in data_numeric:
                        raise rp.RoohyPyException('Key error (alpha): '
                                          + 'data_numeric[^--' + str(a)+']')
                    if fixedparameter not in data_numeric[a]:
                        raise rp.RoohyPyException('Key error (mu): data_numeric['
                                          + str(a) + '][^--'
                                          + str(fixedparameter)+']')
                    item = data_numeric[a][fixedparameter]
                    y.append(item)
        
            if parameter == 'mu':
                x = mus
                for m in mus:
                    if fixedparameter not in data_numeric:
                        raise rp.RoohyPyException('Key error (alpha):'
                                          + 'data_numeric[^--'
                                          + str(fixedparameter)+']')
                    if m not in data_numeric[fixedparameter]:
                        raise rp.RoohyPyException('Key error (mu): data_numeric['
                                          + str(fixedparameter) + '][^--'
                                          + str(m)+']')
                    item = data_numeric[fixedparameter][m]
                    y.append(item)
        except rp.RoohyPyException as inst:
            print(inst)
            x = list()
            y = list()

        longtermfile.close()
        
    return x, y
    

def getparameterbassin(longtermpath=''):
    """
    """
    with open(longtermpath, 'r') as longtermfile:
        data = json.load(longtermfile)
        
        # Transform the initial json with string keys to numeric keys
        data_numeric = {}
        for key1 in data:
            data_numeric[int(key1)] = {}
            for key2 in data[key1]:
                data_numeric[int(key1)][int(key2)] = \
                    rp.tools.countuniquevalue(data[key1][key2],
                                              sensitivity=10000)
        
        bassinx = sorted(data_numeric)
        bassiny = rp.tools.getuniquesecondlevelkeylist(data_numeric)
        bassiny = sorted(bassiny, reverse=True)
        
        # Coordinate transformation (from value to index)
        # y = ax + b (x: bassin and y the integer index inside an image)
        # bassinx has positive slope
        # bassiny has negative slope
        a_x = (len(bassinx)-1) / (max(bassinx) - min(bassinx))
        b_x = - a_x * min(bassinx)
        
        a_y = - (len(bassiny)-1) / (max(bassiny) - min(bassiny))
        b_y = - a_y * max(bassiny)

        K = np.zeros((len(bassiny), len(bassinx)))
        for x_index in bassinx:
            for y_index in bassiny:
            
                x_index_int = round((a_x * x_index) + b_x)
                y_index_int = round((a_y * y_index) + b_y)
                K[y_index_int][x_index_int] = data_numeric[x_index][y_index]

        longtermfile.close()
        
    return K, bassinx, bassiny
    
    
    
    
def getparameterbassinreverse(longtermpath=''):
    """
    """
    with open(longtermpath, 'r') as longtermfile:
        data = json.load(longtermfile)
        
        # Transform the initial json with string keys to numeric keys
        data_numeric = {}
        for key1 in data:
            data_numeric[int(key1)] = {}
            for key2 in data[key1]:
                data_numeric[int(key1)][int(key2)] = \
                    rp.tools.countuniquevalue(data[key1][key2],
                                              sensitivity=10000)
        
        bassinx = sorted(data_numeric)
        bassiny = rp.tools.getuniquesecondlevelkeylist(data_numeric)
        bassiny = sorted(bassiny)
        
        # Coordinate transformation (from value to index)
        # y = ax + b (x: bassin and y the integer index inside an image)
        # bassinx has positive slope
        # bassiny has positive slope
        a_x = (len(bassinx)-1) / (max(bassinx) - min(bassinx))
        b_x = - a_x * min(bassinx)
        
        a_y = (len(bassiny)-1) / (max(bassiny) - min(bassiny))
        b_y = - a_y * min(bassiny)

        K = np.zeros((len(bassiny), len(bassinx)))
        for x_index in bassinx:
            for y_index in bassiny:
            
                x_index_int = round((a_x * x_index) + b_x)
                y_index_int = round((a_y * y_index) + b_y)
                K[y_index_int][x_index_int] = data_numeric[x_index][y_index]

        longtermfile.close()
        
    return K, bassinx, bassiny


